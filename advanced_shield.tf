data "aws_eip" "nat_gw__public_ip" {
  count     = var.aws_shield_enable ? length(var.private_ipv4_subnets) : 0
  public_ip = aws_nat_gateway.natgw[count.index].public_ip
}

resource "aws_shield_protection" "nat_gateway_eips" {
  count        = var.aws_shield_enable ? length(var.private_ipv4_subnets) : 0
  name         = "nat-gateway-eips-${var.envname}"
  resource_arn = "arn:aws:ec2:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:eip-allocation/${data.aws_eip.nat_gw__public_ip[count.index].id}"
}
