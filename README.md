# tf-aws-vpc-natgw

VPC with NAT Gateway - Terraform Module

Gives you a VPC with private + public subnets in each AZ, and (by default) an S3 VPC endpoint

## Terraform version compatibility

| Module version  | Terraform version |
|-----------------|-------------------|
| 4.x.x and above | 0.12.x            |
| 3.x.x and below | 0.11.x            |

## Usage

```js
module "vpc" {
  source                  = "git::ssh://git@gogs.bashton.net/Bashton-Terraform-Modules/tf-aws-vpc-natgw.git"
  name                    = "my-vpc"
  ipv4_cidr               = "10.0.0.0/16"
  public_ipv4_subnets     = [ "10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24" ]
  private_ipv4_subnets    = [ "10.0.11.0/24", "10.0.12.0/24", "10.0.13.0/24" ]
  azs                     = [ "eu-west-1a", "eu-west-1b", "eu-west-1c" ]
  map_public_ip_on_launch = false
  domain                  = "vpc.example.com"
  domain_name_servers     = [ "127.0.0.1", "AmazonProvidedDNS" ]
  dns_resolution          = true
  dns_hostnames           = true
  s3_vpc_endpoint         = true
  dhcp_options            = false
}
```

## Variables

Variables marked with an * are mandatory, the others have sane defaults and can be omitted.

 - `name`* - name of the VPC
 - `azs` - list of AZs in which to distribute subnets
 - `ipv4_cidr`* - IPv4 CIDR block of the VPC
 - `public_ipv4_subnets`* - list of public ipv4 subnet cidrs
 - `private_ipv4_subnets`* - list of private ipv4 subnet cidrs
 - `domain` - domain name to be used within the VPC
 - `domain_name_servers` - list of domain name servers within the VPC
 - `map_public_ip_on_launch` - boolean to map public IP address to machines on public subnets
 - `dns_resolution` - Toggles the AWS "DNS resolution" parameter to enable/disable VPC
   DNS servers
 - `dns_hostnames` - Toggles the AWS "DNS hostnames" parameter to enable/disable DNS
   hostnames within the VPC
 - `s3_vpc_endpoint` - Whether to add an S3 VPC endpoint
 - `public_propagating_vgws` - VPN Gateways allowed to modify the public route table
 - `private_propagating_vgws` - VPN Gateways allowed to modify the private route table
 - `vpc_extra_tags` - Additional tags for VPC resource
 - `public_subnets_extra_tags` - Additional tags for Public Subnet resource
 - `private_subnets_extra_tags` - Additional tags for Private Subnet resource
 - `dhcp_options` - boolean indicating whether to create a DHCP option set
 - `endpoint_interface_services` - List of AWS service names to create endpoint interfaces for (default: empty)
 - `endpoint_interface_az_count` - Number of AZs to use for each endpoint interface (default: 2)
 - `aws_shield_enable` - Enable/disable the use of AWS Shield Advanced
 - `envname` - The name of the environment

## Outputs

 - `vpc_id` - the VPC id
 - `vpc_ipv4_cidr` - assigned CIDR block of the VPC
 - `availability_zones` - comma separated list of availability zones
 - `public_subnets` - list of public subnet ids
 - `public_route_tables` - list of public route table ids
 - `private_subnets` - list of private subnet ids
 - `private_route_tables` - comma separated list of nat route table ids
 - `nat_eips` - list of elastic network ip addresses of nat gateways
 - `nat_ids` - list of resource ID's for the nat gateways
 - `s3_vpc_endpoint` - The prefix_list_id for use in security groups
 - `endpoint_interface_sgs` - Map of AWS service names to security group IDs

# Testing

To install dependencies on your machine, run `bundle install` in the tests/ directory
Run `rake test` in the tests/ directory to run tests
Run `rake destroy` when you've finished testing.  We don't destroy after every test run.

All newly added features should include awspec tests.

# TODO

 - Add ipv6 support once added to Terraform (see https://github.com/hashicorp/terraform/issues/11430)
