resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "${var.name}-igw"
  }
}

resource "aws_subnet" "public" {
  count             = length(var.public_ipv4_subnets)
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = element(var.public_ipv4_subnets, count.index)
  availability_zone = element(var.azs, count.index)

  tags = merge(
    var.public_subnets_extra_tags,
    {
      "Name" = "${var.name}-public"
    },
  )

  map_public_ip_on_launch = var.map_public_ip_on_launch
}

resource "aws_route_table" "public" {
  depends_on = [aws_internet_gateway.igw]
  vpc_id     = aws_vpc.vpc.id

  propagating_vgws = var.public_propagating_vgws

  tags = {
    Name = "${var.name}-public-route"
  }
}

resource "aws_route" "public_default" {
  depends_on             = [aws_route_table.public]
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.igw.id
  route_table_id         = aws_route_table.public.id
}

resource "aws_route_table_association" "public" {
  count          = length(var.public_ipv4_subnets)
  subnet_id      = element(aws_subnet.public.*.id, count.index)
  route_table_id = aws_route_table.public.id
}
