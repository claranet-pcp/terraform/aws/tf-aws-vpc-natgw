## Gateway Endpoints

resource "aws_vpc_endpoint" "s3_vpc_endpoint" {
  count           = var.s3_vpc_endpoint ? 1 : 0
  vpc_id          = aws_vpc.vpc.id
  service_name    = "com.amazonaws.${data.aws_region.current.name}.s3"
  route_table_ids = concat(aws_route_table.public.*.id, aws_route_table.private.*.id)
}

resource "aws_vpc_endpoint" "dynamodb_vpc_endpoint" {
  count           = var.dynamodb_vpc_endpoint ? 1 : 0
  vpc_id          = aws_vpc.vpc.id
  service_name    = "com.amazonaws.${data.aws_region.current.name}.dynamodb"
  route_table_ids = concat(aws_route_table.public.*.id, aws_route_table.private.*.id)
}

## Interface Endpoints

locals {
  services_set = toset(var.endpoint_interface_services)
}

resource "aws_security_group" "endpoint_interfaces" {
  for_each    = local.services_set
  name        = "${var.name}-endpoint-${each.value}"
  description = "Used by ${each.value} service endpoint interfaces"
  vpc_id      = aws_vpc.vpc.id
}

resource "aws_vpc_endpoint" "endpoint_interfaces" {
  for_each            = local.services_set
  service_name        = "com.amazonaws.${data.aws_region.current.name}.${each.value}"
  vpc_id              = aws_vpc.vpc.id
  vpc_endpoint_type   = "Interface"
  private_dns_enabled = true
  subnet_ids          = slice(aws_subnet.private.*.id, 0, var.endpoint_interface_az_count)
  security_group_ids  = [aws_security_group.endpoint_interfaces[each.value].id]
}
