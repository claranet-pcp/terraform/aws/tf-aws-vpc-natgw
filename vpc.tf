resource "aws_vpc" "vpc" {
  cidr_block           = var.ipv4_cidr
  enable_dns_support   = var.dns_resolution
  enable_dns_hostnames = var.dns_hostnames

  tags = merge(
    var.vpc_extra_tags,
    {
      "Name" = var.name
    },
  )
}

resource "aws_vpc_dhcp_options" "vpc" {
  count               = var.dhcp_options ? 1 : 0
  domain_name         = var.domain
  domain_name_servers = var.domain_name_servers

  tags = {
    Name = var.name
  }
}

resource "aws_vpc_dhcp_options_association" "vpc_dhcp" {
  count           = var.dhcp_options ? 1 : 0
  vpc_id          = aws_vpc.vpc.id
  dhcp_options_id = element(aws_vpc_dhcp_options.vpc.*.id, count.index)
}
