require 'spec_helper'

describe vpc ($terraform_output['vpc_id']['value']) do
	it { should be_available }

	$terraform_output['private_route_tables']['value'].each do |route_table_id|
		it { should have_route_table(route_table_id) }
	end

	$terraform_output['public_route_tables']['value'].each do |route_table_id|
		it { should have_route_table(route_table_id) }
	end

end

$terraform_output['private_subnets']['value'].each do |subnet_id|
	describe subnet (subnet_id) do
		it { should have_tag('Name').value('awspec-vpc-natgw-private') }
	end
end

$terraform_output['public_subnets']['value'].each do |subnet_id|
	describe subnet (subnet_id) do
		it { should have_tag('Name').value('awspec-vpc-natgw-public') }
	end
end


$terraform_output['private_route_tables']['value'].each do |route_table_id|
	describe route_table (route_table_id) do
		it { should have_route('10.0.0.0/16').target(gateway: 'local') }
	end
end


$terraform_output['public_route_tables']['value'].each do |route_table_id|
	describe route_table (route_table_id) do
		it { should have_route('10.0.0.0/16').target(gateway: 'local') }
	end
end
