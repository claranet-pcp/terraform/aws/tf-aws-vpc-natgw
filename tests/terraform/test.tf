provider "aws" {
  region = "eu-west-2"
}

module "vpc" {
  source                = "../../"
  name                  = "awspec-vpc-natgw"
  ipv4_cidr             = "10.0.0.0/16"
  public_ipv4_subnets   = ["10.0.1.0/24", "10.0.2.0/24"]
  private_ipv4_subnets  = ["10.0.11.0/24", "10.0.12.0/24"]
  azs                   = ["eu-west-2a", "eu-west-2b"]
  s3_vpc_endpoint       = true
  dynamodb_vpc_endpoint = true
}

output "vpc_id" {
  value = "${module.vpc.vpc_id}"
}

output "eips" {
  value = ["${module.vpc.nat_eips}"]
}

output "private_subnets" {
  value = ["${module.vpc.private_subnets}"]
}

output "public_subnets" {
  value = ["${module.vpc.public_subnets}"]
}

output "private_route_tables" {
  value = ["${module.vpc.private_route_tables}"]
}

output "public_route_tables" {
  value = ["${module.vpc.public_route_tables}"]
}
