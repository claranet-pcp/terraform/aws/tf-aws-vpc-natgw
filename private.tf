resource "aws_eip" "nat" {
  count      = length(var.private_ipv4_subnets)
  depends_on = [aws_internet_gateway.igw]
  domain     = "vpc"
}

resource "aws_nat_gateway" "natgw" {
  count         = length(var.private_ipv4_subnets)
  allocation_id = element(aws_eip.nat.*.id, count.index)
  subnet_id     = element(aws_subnet.public.*.id, count.index)

  tags = merge(
    {
      "Name" = "${var.name}-natgw-${count.index}"
    },
    var.nat_gateway_tags
  )
}

resource "aws_subnet" "private" {
  count             = length(var.private_ipv4_subnets)
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = element(var.private_ipv4_subnets, count.index)
  availability_zone = element(var.azs, count.index)

  tags = merge(
    var.private_subnets_extra_tags,
    {
      "Name" = "${var.name}-private"
    },
  )
}

resource "aws_route_table" "private" {
  count  = length(var.private_ipv4_subnets)
  vpc_id = aws_vpc.vpc.id

  propagating_vgws = var.private_propagating_vgws

  tags = {
    Name = "${var.name}-private-route"
  }
}

resource "aws_route" "private_default" {
  count                  = length(var.private_ipv4_subnets)
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = element(aws_nat_gateway.natgw.*.id, count.index)
  route_table_id         = element(aws_route_table.private.*.id, count.index)
}

resource "aws_route_table_association" "nat" {
  count          = length(var.private_ipv4_subnets)
  subnet_id      = element(aws_subnet.private.*.id, count.index)
  route_table_id = element(aws_route_table.private.*.id, count.index)
}
