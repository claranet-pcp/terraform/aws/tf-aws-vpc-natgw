variable "name" {
}

variable "domain_name_servers" {
  type    = list(string)
  default = ["AmazonProvidedDNS"]
}

# VPC
variable "ipv4_cidr" {
}

variable "domain" {
  default = "eu-west-1.compute.internal"
}

variable "azs" {
  type    = list(string)
  default = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
}

variable "public_ipv4_subnets" {
  type = list(string)
}

variable "private_ipv4_subnets" {
  type = list(string)
}

variable "map_public_ip_on_launch" {
  default = false
}

variable "dns_resolution" {
  default = true
}

variable "dns_hostnames" {
  default = false
}

variable "s3_vpc_endpoint" {
  default = true
}

variable "dynamodb_vpc_endpoint" {
  default = false
}

variable "public_propagating_vgws" {
  type    = list(string)
  default = []
}

variable "private_propagating_vgws" {
  type    = list(string)
  default = []
}

variable "vpc_extra_tags" {
  type    = map(string)
  default = {}
}

variable "public_subnets_extra_tags" {
  type    = map(string)
  default = {}
}

variable "private_subnets_extra_tags" {
  type    = map(string)
  default = {}
}

variable "dhcp_options" {
  description = "Bool indicating whether to create a DHCP option set. This allows the flexibility to create DHCP Option sets outside of the module i.e. AWS Directory Service"
  default     = true
}

variable "endpoint_interface_services" {
  description = "List of AWS service names to create endpoint interfaces for"
  type        = list(string)
  default     = []
}

variable "endpoint_interface_az_count" {
  description = "Number of AZs to use for each endpoint interface"
  type        = number
  default     = 2
}

variable "aws_shield_enable" {
  type        = bool
  description = "Enable/disable the use of AWS Shield Advanced"
  default     = false
}

variable "envname" {
  type    = string
  default = ""
}

variable "nat_gateway_tags" {
  type      = map(string)
  default   = {}
}
