output "vpc_id" {
  value = aws_vpc.vpc.id
}

output "vpc_ipv4_cidr" {
  value = aws_vpc.vpc.cidr_block
}

output "availability_zones" {
  value = aws_subnet.public.*.availability_zone
}

output "public_subnets" {
  value = aws_subnet.public.*.id
}

output "public_route_tables" {
  value = aws_route_table.public.*.id
}

output "private_subnets" {
  value = aws_subnet.private.*.id
}

output "private_route_tables" {
  value = aws_route_table.private.*.id
}

output "nat_eips" {
  value = aws_eip.nat.*.public_ip
}

output "nat_ids" {
  value = aws_nat_gateway.natgw.*.id
}

output "vpc_s3_endpoint" {
  value = aws_vpc_endpoint.s3_vpc_endpoint.*.prefix_list_id
}

output "endpoint_interface_sgs" {
  value = {
    for service, attributes in aws_security_group.endpoint_interfaces:
    service => attributes.id
  }
}

output "aws_shield_protection_id" {
  value = join("", aws_shield_protection.nat_gateway_eips.*.id)
}

output "aws_shield_protection_arn" {
  value = join("", aws_shield_protection.nat_gateway_eips.*.arn)
}
